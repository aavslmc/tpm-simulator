#!/bin/bash

echo -e "\n==== Configuring AAVS Simulator ====\n"

# Currently, AAVS LMC has to be installed in this directory
# DO NOT CHANGE!
export AAVS_INSTALL_DIRECTORY=/opt/aavs

# Create installation directory tree
function create_install() {

  # Create install directory if it does not exist
  if [ -z "$AAVS_INSTALL" ]; then
    export AAVS_INSTALL=$AAVS_INSTALL_DIRECTORY

    # Check whether directory already exists
    if [ ! -d "$AAVS_INSTALL_DIRECTORY" ]; then
      # Check whether we have write persmission
      parent_dir="$(dirname "$AAVS_INSTALL_DIRECTORY")"
      if [ -w "$parent_dir" ]; then
        mkdir -p $AAVS_INSTALL_DIRECTORY
      else
        sudo mkdir -p $AAVS_INSTALL_DIRECTORY
        sudo chown $USER $AAVS_INSTALL_DIRECTORY
      fi
    fi
  fi

  if [ ! -d "$AAVS_INSTALL/lib" ]; then
    mkdir -p $AAVS_INSTALL/lib
  fi

  if [[ ! ":$LD_LIBRARY_PATH:" == *"aavs"* ]]; then
    export LD_LIBRARY_PATH=$AAVS_INSTALL/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH} 
  fi
  
  if [ ! -d "$AAVS_INSTALL/bin" ]; then
    mkdir -p $AAVS_INSTALL/bin
  fi

  if [ -z "$AAVS_BIN" ]; then
    export AAVS_BIN=$AAVS_INSTALL/bin
  fi
}

# Create installation directory
create_install
echo "Created installed directory tree"

# Check if AAVS_PATH exists, and if so cd to it
if [ -z $AAVS_INSTALL ]; then
    echo "AAVS_INSTALL not set in termninal"
    exit 1
else
  # Build library
  cmake -DCMAKE_INSTALL_PREFIX=$AAVS_INSTALL/lib ..
  make -B install
fi
popd
popd

# Install required python packages
pushd python

# Check if we are using python in virtual env, if not we need to install
# numpy via synaptic (not sure why)
if [ `python -c "import sys; print hasattr(sys, 'real_prefix')"` = "False" ]; then  
    sudo apt-get install python-numpy
fi

# pip install -r requirements.pip


# Install simulator python library
python setup.py install

# Finished with python
popd
