from pytpmsimulator.exception import *

random_errors_enabled = False
random_errors_rate = 0

time_speed = 1

function_exceptions = {
    '__init__': [],
    'connect': [],
    'disconnect': [],
    'program_fpgas': [],
    'initialise': [],
    'get_voltage': [],
    'get_adc_power': [],
    'get_temperature': [],
    'get_register_list': [],
    'read_register': [],
    'write_register': [],
    'read_address': [],
    'write_address': [],
    'update_time': [],
    'set_fpga_time': [],
    'get_current': [],
    'get_state': [],
    'is_programmed': []
}

current = {'low': 3, 'high': 5}
voltage = {'low': 4.8, 'high': 5}

devices = [
    {'cpld':
        {'Temperature':
            {
                'min': 45,
                'max': 60,
                'time_to_reach_max': 60
            },

            'Adc_Power':
                {
                    'low': 5,
                    'high': 10
                }
        }
    },

    {'fpga0':
        {'Temperature':
            {
                'min': 50,
                'max': 70,
                'time_to_reach_max': 60
            },

            'Adc_Power':
                {
                    'low': 5,
                    'high': 10
                }
        }
    },

    {'fpga1':
        {'Temperature':
            {
                'min': 50,
                'max': 70,
                'time_to_reach_max': 60
            },

            'Adc_Power':
                {
                    'low': 5,
                    'high': 10
                }
        }
    }
]
