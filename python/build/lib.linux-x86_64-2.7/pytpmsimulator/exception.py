# -*- coding: utf-8 -*-


class InvalidIpException(Exception):
    def __init__(self, msg="Ip is not valid."):
        super(InvalidIpException, self).__init__(msg)


class DownloadFailedException(Exception):
    def __init__(self, msg="Download Failed."):
        super(DownloadFailedException, self).__init__(msg)


class InvalidFileException(Exception):
    def __init__(self, msg="File is invalid."):
        super(InvalidFileException, self).__init__(msg)


class MaxTempExceededException(Exception):
    def __init__(self, temperature):
        msg = "%s°C exceeds the maximum temperature of device." % temperature
        super(MaxTempExceededException, self).__init__(msg)


class MaxCurrentExceededException(Exception):
    def __init__(self, msg="Maximum current has been exceeded."):
        super(MaxCurrentExceededException, self).__init__(msg)


class AdcPowerExceededException(Exception):
    def __init__(self, adc_power):
        msg = "%sW exceeds the maximum adc power." % adc_power
        super(AdcPowerExceededException, self).__init__(msg)


class MaxVoltageExceededException(Exception):
    def __init__(self, voltage):
        msg = "%sV exceeds the maximum voltage of device." % voltage
        super(MaxVoltageExceededException, self).__init__(msg)


class ParseFailedException(Exception):
    def __init__(self, msg="Error occurred whilst parsing file."):
        super(ParseFailedException, self).__init__(msg)


class BoardException(Exception):
    def __init__(self, msg="Board Exception occurred."):
        super(BoardException, self).__init__(msg)


class InitialiseFailedException(Exception):
    def __init__(self, msg="Error occurred during initialisation."):
        super(InitialiseFailedException, self).__init__(msg)


class DisconnectFailedException(Exception):
    def __init__(self, msg="Error occurred during disconnect."):
        super(DisconnectFailedException, self).__init__(msg)


class BootFailedException(Exception):
    def __init__(self, msg="Failed to boot the board."):
        super(BootFailedException, self).__init__(msg)


class FailedToGetRegisterListException(Exception):
    def __init__(self, msg="Failed to retrieve register list."):
        super(FailedToGetRegisterListException, self).__init__(msg)


class RegisterNotFoundException(Exception):
    def __init__(self, msg="Register not found. Make sure name of register and device are correct."):
        super(RegisterNotFoundException, self).__init__(msg)


class InvalidNumOfWordsException(Exception):
    def __init__(self, msg="Number of words needs to be a positive integer"):
        super(InvalidNumOfWordsException, self).__init__(msg)


class AddressNotFoundException(Exception):
    def __init__(self, msg="Address not found."):
        super(AddressNotFoundException, self).__init__(msg)

