from time import sleep
import itertools
import socket
import struct


class IntegratedChannelisedData(object):
    def __init__(self, ip, port, tile_id, station_id, unix_epoch_time):

        self._ip = ip
        self._port = port

        self._unix_epoch_time = unix_epoch_time
        self._timestamp = 0
        self._lmc_capture_mode = 0x6
        self._tpm_id = tile_id
        self._station_id = station_id
        self._packet_payload_length = 1024

        self._nof_fpgas = 2
        self._nof_pols = 2
        self._nof_ants_per_fpga = 8
        self._nof_channels = 512
        self._nof_channels_per_packet = 256

        # Create socket
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def send_data(self, timestamp):
        """ Generate integrated channel data """

        for i in range(self._nof_ants_per_fpga):
            for j in range(self._nof_channels / self._nof_channels_per_packet):
                for k in range(self._nof_fpgas):
                    # At this point we generate an entire packet
                    self._transmit_packet(0, 0, timestamp, i + k * self._nof_ants_per_fpga,
                                          j * self._nof_channels_per_packet)

    def _transmit_packet(self, index, counter, timestamp, start_antenna, start_channel):
        """ Generate a packet """
        header = 0x53 << 56 | 0x04 << 48 | 0x02 << 40 | 0x06 << 32 | 0x08
        heap_counter = 1 << 63 | 0x0001 << 48 | index << 24 | counter
        pkt_len = 1 << 63 | 0x0004 << 48 | self._packet_payload_length
        sync_time = 1 << 63 | 0x1027 << 48 | self._unix_epoch_time
        timestamp &= 0xFFFFFFFFFF
        timestamp |= 1 << 63 | 0x1600 << 48
        lmc_capture_mode = 1 << 63 | 0x2004 << 48 | self._lmc_capture_mode
        lmc_info = 1 << 63 | 0x2002 << 48 | start_channel << 24 | start_antenna << 8 | 1
        lmc_tpm_info = 1 << 63 | 0x2001 << 48 | self._tpm_id << 32 | self._station_id << 16
        sample_offset = 0 << 63 | 0x3300 << 48

        data = range(start_antenna * self._nof_channels + start_channel,
                     start_antenna * self._nof_channels + start_channel + self._nof_channels_per_packet)
        data = list(itertools.chain(*zip(data, data)))

        packet = struct.pack('>' + 'Q' * 9, header, heap_counter, pkt_len, sync_time, timestamp,
                             lmc_capture_mode, lmc_info, lmc_tpm_info, sample_offset) + struct.pack('H' * len(data),
                                                                                                    *data)

        self._socket.sendto(packet, (self._ip, self._port))

# To test, use daq_receiver:
# python daq_receiver.py -i lo -d . -D
if __name__ == "__main__":
    data = IntegratedChannelisedData("127.0.0.1", 4660, 0, 0, 1)
    while True:
        sleep(1)
        data.send_data(1)
