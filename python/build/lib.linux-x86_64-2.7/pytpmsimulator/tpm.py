from pprint import pprint
from pytpmsimulator.exception import *
import logging


class TPM(object):
    def __init__(self, tile):

        self.tile = tile

    def get_register_list(self):
        """ Returns all registers belonging to CPLD and FPGA.
            Only accessible when board is programmed."""
        pprint(dict(self.tile.register_list))
        return True

    def is_programmed(self):
        """ Function that returns true if board is programmed"""
        return self.tile.programmed

    def read_address(self, address, n):
        """ Function that returns the value of a register given its address.
            :param address: Memory address to read from
            :param n: Number of words to read"""

        # Validate number of words i.e. n
        if n < 0:
            logging.error("Number of words needs to be a positive integer")
            return []
        try:
            try:
                # Search for register with given address and store the register's value in values.
                values = (self.tile.register_list[item]['value']
                          for item in self.tile.register_list
                          if self.tile.register_list[item]['address'] == address).next()

                if values is None:
                    logging.info("Register does not have a value")
                    return []

                # Log and return the first n values
                return values[:n] if type(values) is list else values

            except StopIteration:
                raise AddressNotFoundException

        except AddressNotFoundException as e:
            logging.error(e.message)
            return []

        except Exception as e:
            logging.error("Unexpected exception.\nMessage: %s" % e.message)
            return []

    def read_register(self, register_name, n, offset, device):
        """ Function that gets the value of a register given its name and device
            :param register_name: name of register
            :param n: number of words to read
            :param offset: memory address offset to read from
            :param device: device to which the register belongs"""

        try:
            # Validate number of words i.e. n
            if n < 0:
                raise InvalidNumOfWordsException

            # Full name of register i.e. DeviceName.RegisterName
            full_register_name = '%s.%s' % (device, register_name)

            # Set to lower case
            full_register_name = full_register_name.lower()

            # Check if register exists in dictionary
            if full_register_name not in self.tile.register_list:
                raise RegisterNotFoundException

            # Retrieve list of values
            values = self.tile.register_list[full_register_name]['value']

            # Return first n values from the list
            return values[:n] if type(values) is list else values

        except InvalidNumOfWordsException as e:
            logging.error(e.message)

        except RegisterNotFoundException as e:
            logging.error(e.message)

    def write_register(self, register_name, values, offset, device):
        """ Function that writes a list of values to a register
            :param register_name: Name of the register
            :param values: list of values to write to register
            :param offset: address offset of register
            :param device: device to which the register falls under"""

        # No use for offset in simulator

        # Full name of register i.e. DeviceName.RegisterName
        full_register_name = '%s.%s' % (device, register_name)

        # Set to lower case
        full_register_name = full_register_name.lower()

        try:
            # Check if register exists in dictionary
            if full_register_name not in self.tile.register_list:
                raise RegisterNotFoundException

            size = int(self.tile.register_list[full_register_name]['size'])

            # Check is amount of values is equal to register's size
            if len(values) > size:
                logging.error("Number of elements cannot be greater than size of register i.e. %s" % size)
                return False

            # Check if value field is None
            if self.tile.register_list[full_register_name]['value'] is not 0:
                # check if there is enough space to fit values without deleting others
                if len(values) <= size - len(self.tile.register_list[full_register_name]['value']) and values:
                    self.tile.register_list[full_register_name]['value'] += values
                else:
                    # overwrite previous values
                    self.tile.register_list[full_register_name]['value'] = values

            else:
                # Set values
                self.tile.register_list[full_register_name]['value'] = values

        except RegisterNotFoundException as e:
            logging.error(e.message)

        except Exception as e:
            logging.error("Unexpected exception.\nMessage: %s" % e.message)
            return False

        return True