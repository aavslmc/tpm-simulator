import socket
import logging


def validate_ip(ip):
    """Function that checks if a given ip is valid or not
       :param ip: ip to validate"""
    try:
        socket.inet_aton(ip)

    except socket.error:
        return False

    except Exception as e:
        logging.error("Unexpected Error.\nMessage: %s" % e.message)
        return False

    return True


def merge_dictionaries(device_list):
    """ Function to merge a number of dictionaries together
        :param device_list: a list of devices whose registers are to be merged"""

    merged_dicts = {}

    for device in device_list:
        merged_dicts.update(device.registers)

    return merged_dicts
