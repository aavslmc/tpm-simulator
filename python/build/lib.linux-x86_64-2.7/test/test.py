import unittest

import python.pytpmsimulator.config
from python.pytpmsimulator.exception import *
from python.pytpmsimulator.tpmsimulator import TpmSimulator


def _new_simulator():
    return TpmSimulator('10.0.10.2', 10000, '10.0.10.1', 4660)


def _call_program(simulator):
    return simulator.program_fpgas('1712209418_fpga0.xml', 'unit_TestXML', '1712209418_cpld.xml')


def write_address():
    simulator = _new_simulator()
    simulator.connect()
    _call_program(simulator)
    simulator.write_address('invalid address', [4])


def write_register_invalid_device():
    simulator = _new_simulator()
    simulator.connect()
    _call_program(simulator)
    simulator.write_register('regfile.rev', [4], 0, 'invalid device')


def write_register_invalid_name():
    simulator = _new_simulator()
    simulator.connect()
    _call_program(simulator)
    simulator.write_register('invalid register name', [4], 0, 'fpga1')


def read_invalid_address():
    simulator = _new_simulator()
    simulator.connect()
    _call_program(simulator)
    simulator.read_address('invalid', 1), []


class Testing(unittest.TestCase):

    python.pytpmsimulator.config.random_errors_enabled = False
    # ------------------------- CHANGING STATE TESTS -------------------------

    def test_Disc_After_Init(self):
        """Testing normal procedure"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())

    def test_Disc_After_Prog(self):
        """Testing disconnect after programming board i.e. without initialisation"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.disconnect())

    def test_Disc_After_Connect(self):
        """Testing disconnect after connecting i.e. without programming board"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(simulator.disconnect())

    def test_Disc_When_Off(self):
        """Testing disconnect when state is already OFF"""
        simulator = _new_simulator()
        self.assertFalse(simulator.disconnect())

    def test_Prog_When_Off(self):
        """Testing program function when state is OFF"""
        simulator = _new_simulator()
        self.assertFalse(_call_program(simulator))

    def test_Init_When_Off(self):
        """Testing initialise function when state is OFF"""
        simulator = _new_simulator()
        self.assertFalse(simulator.initialise())

    def test_Init_NoProg(self):
        """Testing initialise function after connecting, i.e. still not programmed"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertFalse(simulator.initialise())

    def test_Init_After_Disc(self):
        """Testing initialise after disconnection"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertFalse(simulator.initialise())

    def test_Reconnect(self):
        """Testing a reconnection, i.e. connect (with initialise=False) after disconnect"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertTrue(simulator.connect())

    def test_Reconnect_Auto_Init(self):
        """Testing a reconnection, i.e. connect (with initialise=True) after disconnect"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertTrue(simulator.connect(True))

    def test_isProg(self):
        """Testing is_programmed() at different stages"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertFalse(simulator.is_programmed())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.is_programmed())
        self.assertTrue(simulator.disconnect())
        self.assertTrue(simulator.is_programmed())
        self.assertTrue(simulator.connect())
        self.assertTrue(simulator.is_programmed())

    def test_isProg_Auto_Init(self):
        """Testing is_programmed() at different stages"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertFalse(simulator.is_programmed())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.is_programmed())
        self.assertTrue(simulator.disconnect())
        self.assertTrue(simulator.is_programmed())
        self.assertTrue(simulator.connect(True))
        self.assertTrue(simulator.is_programmed())

    def test_Invalid_Ip(self):
        """Testing IP validation when only 1 is invalid"""
        simulator = TpmSimulator('10.0.10.22222', 10000, '10.0.10.1', 0x248a07463b5e, 4660)
        self.assertRaises(InvalidIpException, simulator.connect())

    def test_Invalid_Ips(self):
        """Testing IP validation when two are invalid"""
        simulator = TpmSimulator('10.0.10.22222', 10000, '10.0.10.111111', 4660)
        self.assertRaises(InvalidIpException, simulator.connect())

    def test_Prog_After_Disc(self):
        """Testing program after disconnect"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertFalse(_call_program(simulator))

    def test_Prog_After_Init(self):
        """Testing program after disconnect"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(_call_program(simulator))

    def test_State(self):
        """Testing get_state function at different stages"""
        simulator = _new_simulator()
        self.assertEqual(simulator.state, 0)
        self.assertTrue(simulator.connect())
        self.assertEqual(simulator.state, 1)
        self.assertTrue(_call_program(simulator))
        self.assertEqual(simulator.state, 2)
        self.assertTrue(simulator.initialise())
        self.assertEqual(simulator.state, 3)
        self.assertTrue(simulator.disconnect())
        self.assertEqual(simulator.state, 0)
        self.assertTrue(simulator.connect())
        self.assertEqual(simulator.state, 2)

    def test_State_Auto_Reconnect(self):
        """Testing get_state function at different stages"""
        simulator = _new_simulator()
        self.assertEqual(simulator.state, 0)
        self.assertTrue(simulator.connect())
        self.assertEqual(simulator.state, 1)
        self.assertTrue(_call_program(simulator))
        self.assertEqual(simulator.state, 2)
        self.assertTrue(simulator.initialise())
        self.assertEqual(simulator.state, 3)
        self.assertTrue(simulator.disconnect())
        self.assertEqual(simulator.state, 0)
        self.assertTrue(simulator.connect(True))
        self.assertEqual(simulator.state, 3)

    # -------------------------- TEMPERATURE TESTS --------------------------

    def test_get_Temp_When_Off(self):
        """Testing attempt to get temperature when state is OFF"""
        simulator = _new_simulator()
        self.assertEqual(simulator.get_temperature(simulator.cpld_device), -1)

    def test_get_Temp_When_Not_Prog(self):
        """Testing attempt to get temperature when state is ON but board is NOT programmed"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertEqual(simulator.get_temperature(simulator.cpld_device), -1)

    def test_get_Temp_When_Prog(self):
        """Testing attempt to get temperature when board is programmed"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertGreater(simulator.get_temperature(simulator.cpld_device), 0)

    def test_get_Temp_When_Init(self):
        """Testing attempt to get temperature when board is initialised"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertGreater(simulator.get_temperature(simulator.cpld_device), 0)

    def test_get_Temp_When_Disc(self):
        """Testing attempt to get temperature after disconnect i.e. state is OFF"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertEqual(simulator.get_temperature(simulator.cpld_device), -1)

    def test_get_Temp_After_Reconnect(self):
        """Testing attempt to get temperature after reconnect
           State of board should be programmed and therefore temperature should be greater than 0"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertTrue(simulator.connect())
        self.assertGreater(simulator.get_temperature(simulator.cpld_device), 0)

    # ---------------------------- REGISTER TESTS ----------------------------

    def test_get_register_list(self):
        """Testing get_register_list function at different stages"""
        simulator = _new_simulator()
        self.assertFalse(simulator.get_register_list())
        self.assertTrue(simulator.connect())
        self.assertFalse(simulator.get_register_list())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.get_register_list())
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.get_register_list())
        self.assertTrue(simulator.disconnect())
        self.assertFalse(simulator.get_register_list())
        self.assertTrue(simulator.connect())
        self.assertTrue(simulator.get_register_list())

    def test_get_register_list_Auto_Reconnect(self):
        """Testing get_register_list function at different stages"""
        simulator = _new_simulator()
        self.assertFalse(simulator.get_register_list())
        self.assertTrue(simulator.connect())
        self.assertFalse(simulator.get_register_list())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.get_register_list())
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.get_register_list())
        self.assertTrue(simulator.disconnect())
        self.assertFalse(simulator.get_register_list())
        self.assertTrue(simulator.connect(True))
        self.assertTrue(simulator.get_register_list())

    # ------------------------- WRITE_REGISTER TESTS -------------------------

    def test_write_register(self):
        """ Testing write_register() function at different stages"""
        simulator = _new_simulator()
        self.assertFalse(simulator.write_register('regfile.rev', [4], 0, 'fpga1'))
        self.assertTrue(simulator.connect())
        self.assertFalse(simulator.write_register('regfile.rev', [4], 0, 'fpga1'))
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_register('regfile.rev', [4], 0, 'fpga1'))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.write_register('regfile.rev', [4], 0, 'fpga1'))
        self.assertTrue(simulator.disconnect())
        self.assertFalse(simulator.write_register('regfile.rev', [4], 0, 'fpga1'))
        self.assertTrue(simulator.connect())
        self.assertTrue(simulator.write_register('regfile.rev', [4], 0, 'fpga1'))

    def test_write_register_Auto_Reconnect(self):
        """ Testing write_register() function at different stages"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertTrue(simulator.connect(True))
        self.assertTrue(simulator.write_register('regfile.rev', [4], 0, 'fpga1'))

    def test_write_register_invalid_name(self):
        """ Testing write_register() with an invalid register name"""
        self.assertRaises(RegisterNotFoundException, write_register_invalid_name())

    def test_write_register_more_values_than_size(self):
        """ Testing write_register() when the amount of values to write is greater than the size of the register.
            In the case below, the register 'regfile.rev' has a size of 1."""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertFalse(simulator.write_register('regfile.rev', [4,2,3], 0, 'fpga1'))

    def test_write_register_no_values(self):
        """ Testing write_register() when the amount of values to write is less than the size of the register.
            In the case below, the register 'regfile.rev' has a size of 1."""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_register('regfile.rev', [], 0, 'fpga1'))

    def test_write_register_invalid_device(self):
        """ Testing write_register() with an invalid device name"""
        self.assertRaises(RegisterNotFoundException, write_register_invalid_device())

    def test_write_register_multiple_values(self):
        """ Testing write_register() function with multiple values. The register specified below has a size of 4.
            Therefore it can only allow to store up to 4 values at any single time."""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_register('regfile.xml_mmap_magic', [2,3], 0, 'fpga1'))
        self.assertEqual(simulator.read_address('0x30000004', 2), [2,3])
        self.assertTrue(simulator.write_register('regfile.xml_mmap_magic', [2,3], 0, 'fpga1'))
        self.assertEqual(simulator.read_address('0x30000004', 4), [2,3,2,3])
        self.assertTrue(simulator.write_register('regfile.xml_mmap_magic', [5], 0, 'fpga1'))
        self.assertEqual(simulator.read_address('0x30000004', 4), [5])
        self.assertTrue(simulator.write_register('regfile.xml_mmap_magic', [1,1,1,1], 0, 'fpga1'))
        self.assertEqual(simulator.read_address('0x30000004', 4), [1,1,1,1])
        self.assertFalse(simulator.write_register('regfile.xml_mmap_magic', [1,1,1,1,1], 0, 'fpga1'))

    # ------------------------- WRITE_ADDRESS TESTS -------------------------

    def test_write_address(self):
        """ Testing write_address() function at different stages"""
        simulator = _new_simulator()
        self.assertFalse(simulator.write_address('0x300e1e18', [4]))
        self.assertTrue(simulator.connect())
        self.assertFalse(simulator.write_address('0x300e1e18', [4]))
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0x300e1e18', [4]))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.write_address('0x300e1e18', [4]))
        self.assertTrue(simulator.disconnect())
        self.assertFalse(simulator.write_address('0x300e1e18', [4]))
        self.assertTrue(simulator.connect())
        self.assertTrue(simulator.write_address('0x300e1e18', [4]))

    def test_write_address_Auto_Reconnect(self):
        """ Testing write_address() function at different stages"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertTrue(simulator.connect(True))
        self.assertTrue(simulator.write_address('0x300e1e18', [4]))

    def test_write_address_invalid_address(self):
        """ Testing write_address() when the address is invalid"""
        self.assertRaises(AddressNotFoundException, write_address())

    def test_write_address_no_values(self):
        """ Testing write_address() when the amount of values is less than the size of the register.
            In the case below, the register at address 0xe1c28 has a size of 1."""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0xe1c28', []))

    def test_write_address_more_values_than_size(self):
        """ Testing write_address() when the amount of values is greater than the size of the register.
            In the case below, the register at address 0xe1c28 has a size of 1."""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertFalse(simulator.write_address('0xe1c28', [1,2,3]))

    def test_write_address_multiple_values(self):
        """ Testing write_address() function with multiple values.
            The address specified below has a size of 4. Therefore it should only allow up to 4 values."""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0x30000004', [2,3]))
        self.assertEqual(simulator.read_address('0x30000004', 2), [2,3])
        self.assertTrue(simulator.write_address('0x30000004', [2, 3]))
        self.assertEqual(simulator.read_address('0x30000004', 4), [2,3,2,3])
        self.assertTrue(simulator.write_address('0x30000004', [5]))
        self.assertEqual(simulator.read_address('0x30000004', 4), [5])
        self.assertTrue(simulator.write_address('0x30000004', [1,1,1,1]))
        self.assertEqual(simulator.read_address('0x30000004', 4), [1,1,1,1])
        self.assertFalse(simulator.write_address('0x30000004', [1,1,1,1,1]))

    # ------------------------- READ_REGISTER TESTS -------------------------

    def test_read_register_no_value(self):
        """ Testing read_register() when there is no value written to that register, at different board states"""
        simulator = _new_simulator()
        self.assertFalse(simulator.read_register('regfile.rev', 1, 0, 'fpga1'))
        self.assertTrue(simulator.connect())
        self.assertFalse(simulator.read_register('regfile.rev', 1, 0, 'fpga1'))
        self.assertTrue(_call_program(simulator))
        self.assertEqual(simulator.read_register('regfile.rev', 1, 0, 'fpga1'), 0)
        self.assertTrue(simulator.initialise())
        self.assertEqual(simulator.read_register('regfile.rev', 1, 0, 'fpga1'), 0)
        self.assertTrue(simulator.disconnect())
        self.assertFalse(simulator.read_register('regfile.rev', 1, 0, 'fpga1'))
        self.assertTrue(simulator.connect())
        self.assertEqual(simulator.read_register('regfile.rev', 1, 0, 'fpga1'), 0)

    def test_read_register_no_value_Auto_Reconnect(self):
        """ Testing read_register() when there is no value written to that register, at different board states"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertTrue(simulator.connect(True))
        self.assertEqual(simulator.read_register('regfile.rev', 1, 0, 'fpga1'), 0)

    def test_read_register_with_value(self):
        """ Testing read_register() when there is a value written to that register, at different board states"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0x0', [4]))
        self.assertEqual(simulator.read_register('regfile.rev', 1, 0, 'fpga0'), [4])
        self.assertTrue(simulator.initialise())
        self.assertEqual(simulator.read_register('regfile.rev', 1, 0, 'fpga0'), [4])
        self.assertTrue(simulator.disconnect())
        self.assertFalse(simulator.read_register('regfile.rev', 1, 0, 'fpga0'))
        self.assertTrue(simulator.connect())
        self.assertEqual(simulator.read_register('regfile.rev', 1, 0, 'fpga0'), [4])

    def test_read_register_with_value_Auto_Reconnect(self):
        """ Testing read_register() when there is a value written to that register, at different board states"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0x0', [4]))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertTrue(simulator.connect(True))
        self.assertEqual(simulator.read_register('regfile.rev', 1, 0, 'fpga0'), [4])

    def test_read_register_invalid_register_name(self):
        """ Testing read_register() when the register name is invalid"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0x0', [4]))
        self.assertRaises(RegisterNotFoundException, simulator.read_register('invalid name', 1, 0, 'fpga1'))

    def test_read_register_invalid_device_name(self):
        """ Testing read_register() when the device name is invalid"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0x0', [4]))
        self.assertRaises(RegisterNotFoundException, simulator.read_register('regfile.rev', 1, 0, 'invalid device'))

    def test_read_register_invalid_numOfWords(self):
        """ Testing read_register() when the number of words is invalid"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0x0', [4]))
        self.assertRaises(InvalidNumOfWordsException, simulator.read_register('regfile.rev', -1, 0, 'fpga1'))

    # -------------------------- READ_ADDRESS TESTS --------------------------

    def test_read_address_no_value(self):
        """ Testing read_address() when there is no value written to that address, at different board states"""
        simulator = _new_simulator()
        self.assertFalse(simulator.read_address('0x300e1e18', 1))
        self.assertTrue(simulator.connect())
        self.assertFalse(simulator.read_address('0x300e1e18', 1))
        self.assertTrue(_call_program(simulator))
        self.assertEqual(simulator.read_address('0x300e1e18', 1), 0)
        self.assertTrue(simulator.initialise())
        self.assertEqual(simulator.read_address('0x300e1e18', 1), 0)
        self.assertTrue(simulator.disconnect())
        self.assertFalse(simulator.read_address('0x300e1e18', 1))
        self.assertTrue(simulator.connect())
        self.assertEqual(simulator.read_address('0x300e1e18', 1), 0)

    def test_read_address_no_value_Auto_Reconnect(self):
        """ Testing read_address() when there is no value written to that address, at different board states"""
        simulator = _new_simulator()
        self.assertFalse(simulator.read_address('0x300e1e18', 1))
        self.assertTrue(simulator.connect())
        self.assertFalse(simulator.read_address('0x300e1e18', 1))
        self.assertTrue(_call_program(simulator))
        self.assertEqual(simulator.read_address('0x300e1e18', 1), 0)
        self.assertTrue(simulator.initialise())
        self.assertEqual(simulator.read_address('0x300e1e18', 1), 0)
        self.assertTrue(simulator.disconnect())
        self.assertFalse(simulator.read_address('0x300e1e18', 1))
        self.assertTrue(simulator.connect(True))
        self.assertEqual(simulator.read_address('0x300e1e18', 1), 0)

    def test_read_address_with_value(self):
        """ Testing read_address() when there is a value written to that address, at different board states"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0x300e1e18', [4]))
        self.assertEqual(simulator.read_address('0x300e1e18', 1), [4])
        self.assertTrue(simulator.initialise())
        self.assertEqual(simulator.read_address('0x300e1e18', 1), [4])
        self.assertTrue(simulator.disconnect())
        self.assertFalse(simulator.read_address('0x300e1e18', 1))
        self.assertTrue(simulator.connect())
        self.assertEqual(simulator.read_address('0x300e1e18', 1), [4])

    def test_read_address_with_value_Auto_Reconnect(self):
        """ Testing read_address() when there is a value written to that address, at different board states"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0x300e1e18', [4]))
        self.assertTrue(simulator.initialise())
        self.assertTrue(simulator.disconnect())
        self.assertTrue(simulator.connect(True))
        self.assertEqual(simulator.read_address('0x300e1e18', 1), [4])

    def test_read_address_invalid_address(self):
        """ Testing read_address() when the address is invalid"""
        self.assertRaises(AddressNotFoundException, read_invalid_address())

    def test_read_address_invalid_numOfWords(self):
        """ Testing read_address() when the number of words is invalid"""
        simulator = _new_simulator()
        self.assertTrue(simulator.connect())
        self.assertTrue(_call_program(simulator))
        self.assertTrue(simulator.write_address('0xe1c28', [4]))
        self.assertEqual(simulator.read_address('0xe1c28', -1), [])

if __name__ == '__main__':
    unittest.main()
