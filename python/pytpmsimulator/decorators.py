import logging
import numbers
import random

import pytpmsimulator.config as config
from pytpmsimulator.exception import BootFailedException

states_desc = ["OFF", "ON", "PROGRAMMED", "INITIALISED"]


def accepts(return_type, *states):
    """ Decorator to check if the function can be called in the current state.
        :param return_type: defines the return type of the original function
        :param states: the states in which the function can be called"""
    def check_accepts(func):
        def new_func(*args, **kwargs):
            return func(*args, **kwargs)
            # # check if current state of board is in list of states
            #
            # if args[0].state in states:
            #     return func(*args, **kwargs)
            #
            # logging.error("Cannot %s if board is %s" % (func.func_name.upper(), states_desc[args[0].state]))
            #
            # if return_type == bool:
            #     return False
            #
            # if return_type == list:
            #     return []
            #
            # if return_type == dict:
            #     return {}
            #
            # return -1

        new_func.func_name = func.func_name

        return new_func

    return check_accepts


def throws_exceptions(rate, *exceptions):
    """ Decorator that can raise an exception at random according to the rate and if random errors are enabled in files
        :param rate: random rate at which exceptions are raised
        :param exceptions: list of exceptions that can be thrown"""

    def check_throws(func):

        name = func.__name__

        def wrapper(*args, **kwargs):
            try:
                # check if random errors is enabled in files file
                if config.random_errors_enabled:

                    # check if we want to force a particular exception specified in files file
                    if len(config.function_exceptions[func.func_name]) is not 0:

                        # iterate through all exception tuples (rate, exception)
                        for exception in config.function_exceptions[func.func_name]:

                            if random.uniform(0, 1) < exception[0]:
                                raise exception[1]
                        # choose exception at random
                        # raise random.choice(config.function_exceptions[func.__name__])

                    # copy contents of exceptions and rate
                    list_of_exceptions = exceptions
                    error_rate = rate

                    # check if rate is integer/float
                    if not isinstance(rate, numbers.Real):
                        # if not int or float, then rate contains first exception
                        # therefore add it to the list_of_exceptions
                        list_of_exceptions += (rate,)

                        # set error rate to default rate specified in files file
                        error_rate = config.random_errors_rate

                    # check if exception should be raise
                    if random.uniform(0, 1) < error_rate:
                        raise random.choice(list_of_exceptions)

                # return function
                return func(*args, **kwargs)

            # if key error of dictionary is invalid
            except KeyError:
                logging.error("Invalid dictionary key. %s" % func.__name__)

            # we don't just want to log, this will only occur on __init__, therefore we exit
            except BootFailedException:
                raise BootFailedException

            # Handle Exceptions, display message specified in exception class
            except Exception as e:
                logging.error(e.message)

        return wrapper

    return check_throws
