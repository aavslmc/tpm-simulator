# -*- coding: utf-8 -*-

import logging
import math
import random
import time
from abc import abstractmethod

import pytpmsimulator.config as config
from pytpmsimulator.ten_g_core import TpmTenGCore

from enum import Enum


class DeviceID(Enum):
    FPGA_1 = 1
    FPGA_2 = 2


class Device(object):
    FPGA_1 = DeviceID.FPGA_1
    FPGA_2 = DeviceID.FPGA_2

    def __init__(self, key, index):
        self.key = key
        self.index = index
        self.temp_disconnect = None
        self.time = time.time()
        self.structure = None
        self.registers = {}

    @staticmethod
    @abstractmethod
    def return_function_result(x):
        """ Temperatures are calculated based on the graph below.
            :param x: time that board has been programmed"""
        pass

    @staticmethod
    @abstractmethod
    def return_inverse_function(y):
        """ Returns time, used to determine at which second the board was disconnected given the
            temperature when disconnected.
            ":param y: the temperature when disconnected"""
        pass

    @staticmethod
    def _get_time(device, y):
        """ Function that returns the respective time (x) of a particular temperature y
            :param device: Different devices have different graph functions. Therefore we need to know with
            which device we are dealing with to get the correct values.
            :param y: The temperature of the device"""

        # Inverse function of graph that calculates the temperature, returns time
        a = device.return_inverse_function(y)

        try:
            a = a * config.devices[device.index][device.key]['Temperature']['time_to_reach_max'] / 60
            return a

        except ZeroDivisionError as e:
            logging.error("Division by zero.\nMessage: %s" % e.message)
            return -1

        except KeyError as e:
            logging.error("Invalid dictionary key.\nMessage: %s" % e.message)
            return -1

        except Exception as e:
            logging.error("Unexpected Error.\nMessage: %s" % e.message)
            return -1

    def _calculate_temp(self, current_time, time_start, time_disconnected):
        """ Function to calculate current temperature of device
            :param current_time: current time
            :param time_start: time since board is PROGRAMMED / RECONNECTED
            :param time_disconnected: duration of board being in a disconnected state"""

        try:
            if 0 < time_disconnected < \
                    config.devices[self.index][self.key]['Temperature']['time_to_reach_max']:

                try:
                    if float(self.temp_disconnect) > \
                                    config.devices[self.index][self.key]['Temperature']['max'] - 0.1:

                        x = current_time - time_start
                        x = x + \
                            (config.devices[self.index][self.key]['Temperature'][
                                 'time_to_reach_max'] - time_disconnected)
                    else:
                        x = current_time - time_start + self._get_time(self, self.temp_disconnect)

                except ValueError:
                    logging.error("Error getting temperature.")
                    return -1
            else:
                x = current_time - time_start

            try:
                # mapping x to a range of 0-60 values
                x = x / config.devices[self.index][self.key]['Temperature']['time_to_reach_max'] * 60

            except ZeroDivisionError as e:
                logging.error("Divide by Zero occurred.\nMessage: %s" % e.message)
                return -1

            # get temperature based on arctan graph
            temp = self.return_function_result(x)

            # add some randomness
            temperature = random.uniform(temp - 0.1, temp + 0.1)

            return temperature

        except KeyError as e:
            logging.error("Invalid dictionary key.\nMessage: %s" % e.message)
            return -1

        except Exception as e:
            logging.error("Unexpected Error.\nMessage: %s" % e.message)
            return -1

    def _has_reached_max_temp(self, time_start, time_disconnected):
        """ Function that determines whether maximum temperature has been reached or not """

        time_running = time.time() - time_start

        try:
            if time_disconnected != 0:

                if time_disconnected >= \
                        config.devices[self.index][self.key]['Temperature']['time_to_reach_max']:
                    """If the time the board has spent disconnected is greater than
                       the time it takes to cool, then the board has reached its minimum temperature.
                       Therefore, the time left to reach the maximum temp is the time specified in the
                       configuration file  i.e. time it takes from min temp to max temp"""

                    time_left = config.devices[self.index][self.key]['Temperature']['time_to_reach_max']

                else:

                    if config.devices[self.index][self.key]['Temperature']['max'] - 0.1 \
                            <= self.temp_disconnect:
                        """Assuming time it takes to cool = time it takes to heat, then time left should 
                           be equal to the total time disconnected."""
                        time_left = time_disconnected

                    else:
                        """Otherwise, from the inverse of the original arctan graph, we need to know
                           the x value (the time) of the temperature when disconnected.
                           If we subtract this by the time to reach the maximum temp, we get the amount of time
                           left to reach max temp"""
                        time_left = (config.devices[self.index][self.key]['Temperature'][
                                         'time_to_reach_max']) - self._get_time(self, self.temp_disconnect)

                if time_running < time_left:
                    return False

                return True

            else:
                return False if time_running < config.devices[self.index][self.key]['Temperature'][
                    'time_to_reach_max'] else True

        except KeyError as e:
            logging.error("Invalid dictionary key.\nMessage: %s" % e.message)
            return False

        except Exception as e:
            logging.error("Unexpected Error.\nMessage: %s" % e.message)
            return False

    def get_temperature(self, time_start, time_disconnected):
        if not self._has_reached_max_temp(time_start, time_disconnected):
            # Maximum temperature has not yet been reached.
            # Therefore calculate temperature based on time running
            temperature = self._calculate_temp(time.time(), time_start, time_disconnected)
            return temperature

        else:
            try:
                # If board has reached maximum temperature, no need to calculate from function.
                # Therefore set temperature to the max temp as configured by files.by and add include some randomness
                temperature = random.uniform(config.devices[self.index][self.key]['Temperature']['max'] - 0.1,
                                             config.devices[self.index][self.key]['Temperature']['max'] + 0.1)
                return temperature

            except KeyError as e:
                logging.error("Invalid dictionary key.\nMessage: %s" % e.message)
                return -1

            except Exception as e:
                logging.error("Unexpected Error.\nMessage: %s" % e.message)
                return -1

    def get_voltage(self):
        """Get Voltage
           Voltage can only be accessible when running"""

        try:
            voltage = random.uniform(config.devices[self.index][self.key]['Voltage']['low'],
                                     config.devices[self.index][self.key]['Voltage']['high'])
            return round(voltage, 2)

        except KeyError as e:
            logging.error("Invalid dictionary key.\nMessage: %s" % e.message)
            return -1
        except Exception as e:
            logging.error("Unexpected Error.\nMessage: %s" % e.message)
            return -1

    def get_adc_power(self, adc_id):
        """Get ADC Power
           ADC Power can only be accessible when running"""

        if not 0 <= adc_id < 32:
            logging.error("Adc_Id not in range: 0 - 32")
            return -1

        try:
            power = random.uniform(config.devices[self.index][self.key]['Adc_Power']['low'] - 0.1,
                                   config.devices[self.index][self.key]['Adc_Power']['high'])
            return round(power, 2)

        except KeyError as e:
            logging.error("Invalid dictionary key.\nMessage: %s" % e.message)
            return -1

        except Exception as e:
            logging.error("Unexpected Error.\nMessage: %s" % e.message)
            return -1

    def update_device_registers(self):
        """ Iterate through device's structure and store its registers and their info"""
        try:
            for key, value in self.structure['components'].iteritems():

                for register, dictionary in self.structure['components'][key]['registers'].iteritems():
                    self.registers.update({register: dictionary})

        except KeyError as e:
            logging.error("Invalid dictionary key.\nMessage: %s" % e.message)

        except Exception as e:
            logging.error("Unexpected Error.\nMessage: %s" % e.message)

    def set_device_structure(self, structure):
        """ Gets structure of device
            :param structure: xml structure of device"""
        self.structure = structure


class CPLD(Device):
    def __init__(self, key, index):
        Device.__init__(self, key, index)

    @staticmethod
    def return_function_result(x):
        """ Temperatures are calculated based on the graph below.
            :param x: time that board has been programmed"""
        return 6 * (math.atan(0.1 * x - 2.95)) + 52.5

    @staticmethod
    def return_inverse_function(y):
        """ Returns time, used to determine at which second the board was disconnected given the
            temperature when disconnected.
            ":param y: the temperature when disconnected"""
        return 29.5 - 10 * (math.tan(8.75 - (0.166667 * y)))


class FPGA(Device):
    def __init__(self, key, index, board):
        Device.__init__(self, key, index)

        self.tpm_10g_core = []

        self.initial_time_set = None
        self.time_set = 0

        self.time_value = 0

        for i in range(0, 4):
            self.tpm_10g_core.append(TpmTenGCore(board, self, i))

    def set_time(self, device_time):
        self.time_value = device_time
        # if self.initial_time_set is None:
        #     self.initial_time_set = device_time
        #     self.time_set = device_time
        #     self.time_value = device_time
        # else:
        #     self.time_set = device_time
        #     self.time_value = self.time_value + (self.time_set - self.initial_time_set)

    @staticmethod
    def return_function_result(x):
        """ Temperatures are calculated based on the graph below.
            :param x: time that board has been programmed"""
        return 8 * (math.atan(0.1 * x - 2.95)) + 60

    @staticmethod
    def return_inverse_function(y):
        """ Returns time, used to determine at which second the board was disconnected given the
            temperature when disconnected.
            ":param y: the temperature when disconnected"""
        return 29.5 - 10 * (math.tan(7.5 - (0.125 * y)))
