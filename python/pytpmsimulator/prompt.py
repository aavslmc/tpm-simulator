# -*- coding: utf-8 -*-
import logging
import ast

import sys

from device import Device
from tpmsimulator import TpmSimulator

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

logging.info("Type /exit to exit the command prompt.")

try:
    simulator = TpmSimulator('10.0.10.2', 10000, '10.0.10.1', 4660)
except Exception as e:
    logging.error(e.message)
    sys.exit(0)

# Commands to get information from TPM
while True:
    input_string = raw_input("tpm-command> ")

    command_and_args = input_string.split(' ', 1)

    command = command_and_args[0]
    args = command_and_args[1].split() if len(command_and_args) > 1 else None

    if command == 'get_state':

        """Get State of board 
           State can be accessed at any time"""

        logging.info(simulator.get_state())

    elif command == 'get_temp':
        temperature = simulator.get_temperature()
        logging.info("%s°C" % temperature)

    elif command == 'get_fpga0_temp':
        temperature = simulator.get_fpga0_temperature()

        if temperature != -1:
            logging.info("%s°C" % temperature)

    elif command == 'get_fpga1_temp':
        temperature = simulator.get_fpga1_temperature()

        if temperature != -1:
            logging.info("%s°C" % temperature)

    elif command == 'get_voltage':

        voltage = simulator.get_voltage()

        if voltage != -1:
            logging.info("%sV" % voltage)

    elif command == 'get_ip':

        logging.info(simulator.get_ip())

    elif command == 'get_adc_power':
        simulator.get_adc_power(id)

    elif command == 'disconnect':
        simulator.disconnect()

    elif command == 'connect':
        simulator.connect(initialise=True)

    elif command == 'program':
        simulator.program_fpgas('files/bitfile')

    elif command == 'initialise':
        simulator.initialise()

    elif command == 'is_programmed':
        logging.info(simulator.is_programmed())

    elif command == 'get_register_list':
        simulator.get_register_list()

    elif command == 'read_register':
        if len(args) != 4:
            logging.error("Insufficient number of arguments: Expected format (string, int, int, string)")
            continue

        try:
            register_name = args[0]
            n = int(args[1])
            offset = int(args[2])
            device = args[3]

            logging.info("Values: %s" % simulator.read_register(register_name, n, offset, device))

        except Exception as e:
            logging.error("Error parsing arguments: Expected format (string,int,int,string).\nMessage: %s" % e.message)

    elif command == 'read_address':
        if len(args) != 2:
            logging.error("Insufficient number of arguments: Expected format (hex string,int)")
            continue

        try:
            address = args[0]
            n = int(args[1])

            logging.info("Values: %s" % simulator.read_address(address, n))

        except Exception as e:
            logging.error("Error parsing arguments: Expected format (hex string,int).\nMessage: %s" % e.message)

    elif command == 'write_register':
        if len(args) != 4:
            logging.error("Insufficient number of arguments: Expected format (string, list, int, string)")
            continue
        try:
            register_name = args[0]

            if type(ast.literal_eval(args[1])) is not list:
                raise TypeError()

            values = ast.literal_eval(args[1])
            offset = int(args[2])
            device = args[3]

            simulator.write_register(register_name, values, offset, device)

        except Exception as e:
            logging.error("Error parsing arguments: Expected format (string,list,int,string).\nMessage: %s" % e.message)

    elif command == 'write_address':
        if len(args) != 2:
            logging.error("Insufficient number of arguments: Expected format (hex string, list)")
            continue

        try:
            address = args[0]

            if type(ast.literal_eval(args[1])) is not list:
                raise TypeError("Error parsing arguments: Expected format (hex string, list)")

            values = ast.literal_eval(args[1])
            simulator.write_address(address, values)

        except Exception as e:
            logging.error("Message: %s" % e.message)

    elif command == 'start_acquisition':
        if len(args) != 2:
            logging.error("Insufficient number of arguments: Expected format (int, int)")
            continue

        try:
            start_time = int(args[0])
            delay = int(args[1])

            simulator.start_acquisition(start_time, delay)

        except ValueError:
            logging.error("Error parsing arguments: Expected format (int, int)")

    elif command == 'stop_data_transmission':
        simulator.stop_data_transmission()

    elif command == 'get_fpga0_time':
        logging.info(simulator.get_fpga_time(Device.FPGA_1))

    elif command == 'get_fpga1_time':
        logging.info(simulator.get_fpga_time(Device.FPGA_2))

    elif command == 'get_fpga0_timestamp':
        logging.info(simulator.get_fpga_timestamp(simulator.fpga0_device))

    elif command == 'get_fpga1_timestamp':
        logging.info(simulator.get_fpga_timestamp(simulator.fpga1_device))

    elif command == 'get_phase_terminal_count':
        logging.info(simulator.get_phase_terminal_count())

    elif command == 'set_phase_terminal_count':

        if len(args) != 1:
            logging.error("Insufficient number of arguments: Expected format (string, int, int, string)")
            continue

        value = args[0]

        simulator.set_phase_terminal_count(value)

    elif command == 'get_pps_delay':

        logging.info(simulator.get_pps_delay())

    elif command == 'configure_10g_core':

        if len(args) != 7:
            logging.error("Insufficient number of arguments: Expected format (string, int, int, string)")
            continue

        core_id = int(args[0])
        src_mac = int(args[1])
        src_ip = args[2]
        dst_mac = int(args[3])
        dst_ip = args[4]
        src_port = int(args[5])
        dst_port = int(args[6])

        logging.info(simulator.configure_10g_core(core_id, src_mac, src_ip, dst_mac, dst_ip, src_port, dst_port))

    elif command == 'get_10g_core_configuration':

        core_id = int(args[0])

        logging.info(simulator.get_10g_core_configuration(core_id))

    elif command == 'get_current':
        current = simulator.get_current()

        if current != -1:
            logging.info("%sA" % current)

    elif command == '/exit':
        simulator.timer.cancel()
        break

    else:
        logging.info("Unknown Command")
