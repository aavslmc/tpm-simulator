import socket
import struct


class TpmTenGCore(object):
    def __init__(self, board, device, core):
        self.board = board
        self._device = device
        self._core = core

    def set_src_mac(self, mac):
        """ Set source MAC address
            :param mac: MAC address"""
        self.board.register_list['%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_src_mac_addr_lower'
                                 % (self._device.key, self._core)]['value'] = mac & 0xFFFFFFFF
        self.board.register_list['%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_src_mac_addr_upper'
                                 % (self._device.key, self._core)]['value'] = (mac >> 32) & 0xFFFFFFFF
        self.board.register_list['%s.mii_test_ic.core%d_src_mac_lo'
                                 % (self._device.key, self._core)]['value'] = mac & 0xFFFFFFFF
        self.board.register_list['%s.mii_test_ic.core%d_src_mac_hi'
                                 % (self._device.key, self._core)]['value'] = (mac >> 32) & 0xFFFFFFFF

    def get_src_mac(self):
        """ Get source MAC address """
        lower = self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_src_mac_addr_lower'
            % (self._device.key, self._core)]['value']
        upper = self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_src_mac_addr_upper'
            % (self._device.key, self._core)]['value']
        return hex(upper << 32 | lower)

    def set_dst_mac(self, mac):
        """ Set destination MAC address
            :param mac: MAC address"""
        self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_dst_mac_addr_lower'
            % (self._device.key, self._core)]['value'] = mac & 0xFFFFFFFF
        self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_dst_mac_addr_upper'
            % (self._device.key, self._core)]['value'] = (mac >> 32) & 0xFFFFFFFF
        self.board.register_list[
            '%s.mii_test_ic.core%d_dst_mac_lo'
            % (self._device.key, self._core)]['value'] = mac & 0xFFFFFFFF
        self.board.register_list[
            '%s.mii_test_ic.core%d_dst_mac_hi'
            % (self._device.key, self._core)]['value'] = (mac >> 32) & 0xFFFFFFFF

    def get_dst_mac(self):
        """ Get destination MAC address """
        lower = self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_dst_mac_addr_lower'
            % (self._device.key, self._core)]['value']
        upper = self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_dst_mac_addr_upper'
            % (self._device.key, self._core)]['value']
        return hex(upper << 32 | lower)

    def set_src_ip(self, ip):
        """ Set source IP address
            :param ip: IP address"""
        try:
            if type(ip) is not int:
                ip = struct.unpack("!L", socket.inet_aton(ip))[0]
            self.board.register_list[
                '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_udp_src_ip_addr'
                % (self._device.key, self._core)]['value'] = ip
        except:
            raise Exception

    def get_src_ip(self):
        """ Get source IP address """
        ip = self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_udp_src_ip_addr'
            % (self._device.key, self._core)]['value']

        return socket.inet_ntoa(struct.pack('!L', ip))

    def set_dst_ip(self, ip):
        """ Set source IP address
            :param ip: IP address"""
        try:
            ip = struct.unpack("!L", socket.inet_aton(ip))[0]
            self.board.register_list[
                '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_udp_dst_ip_addr' % (
                self._device.key, self._core)]['value'] = ip
        except:
            raise Exception("Tpm10GCore: Could not set destination IP")

    def get_dst_ip(self):
        """ Get destination ip"""
        ip = self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_udp_dst_ip_addr' % (
            self._device.key, self._core)]['value']

        return socket.inet_ntoa(struct.pack('!L', ip))

    def set_src_port(self, port):
        """ Set source IP address
            :param port: Port"""
        self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_udp_ports.udp_src_port' % (
                self._device.key, self._core)]['value'] = port

    def get_src_port(self):
        """ Get source IP address"""
        return self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_udp_ports.udp_src_port' % (
            self._device.key, self._core)]['value']

    def set_dst_port(self, port):
        """ Set source IP address
            :param port: Port"""
        self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_udp_ports.udp_dst_port' % (
            self._device.key, self._core)]['value'] = port

    def get_dst_port(self):
        """ Get destination port"""
        return self.board.register_list[
            '%s.udp_core.udp_core_inst_%d_udp_core_control_udp_core_control_udp_ports.udp_dst_port' % (
            self._device.key, self._core)]['value']
