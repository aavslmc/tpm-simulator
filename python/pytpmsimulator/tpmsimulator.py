# -*- coding: utf-8 -*-
import logging
import random
import time
from pprint import pprint
from threading import Timer

import pytpmsimulator.config as config
import pytpmsimulator.utility as utility
import pytpmsimulator.xmlParse as xmlParse
from pytpmsimulator.spead_channel import IntegratedChannelisedData
from pytpmsimulator.decorators import accepts, throws_exceptions
from pytpmsimulator.device import CPLD, FPGA, Device
from pytpmsimulator.exception import *

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


class State(object):
    OFF = 0
    ON = 1
    PROGRAMMED = 2
    INITIALISED = 3


class TpmSimulator(object):
    @throws_exceptions(BootFailedException)
    def __init__(self, ip="10.0.10.2", port=10000, lmc_ip="127.0.0.1", lmc_mac=0x248a07463b5e, lmc_port=4660):

        print "Testing"
        self.phase_terminal_count = None
        self.ip = ip
        self.port = port
        self.lmc_ip = lmc_ip
        self.lmc_mac = lmc_mac
        self.lmc_port = lmc_port

        self.station_id = None
        self.tile_id = None
        self.epoch_time = 0

        # Set default STATE to off and not programmed
        self.state = State.OFF
        self.programmed = False

        # Time when started
        self.time_start = 0

        # Time when disconnected
        self.t_disc = 0

        self.data = None

        # Total amount of time disconnected
        self.time_disconnected = 0

        # Initialise devices with files key and index
        self.cpld_device = CPLD('cpld', 0)
        self.fpga0_device = FPGA('fpga0', 1, self)
        self.fpga1_device = FPGA('fpga1', 2, self)

        self.device_list = [self.cpld_device, self.fpga0_device, self.fpga1_device]

        # Initialise register list
        self.register_list = {}

        self.data_timer = None
        self.timer = None

    @throws_exceptions(BoardException, InvalidIpException)
    def connect(self, initialise=False, simulation='', enable_ada=''):
        """ Start connection
            :param initialise: If true, proceed to initialise() if already programmed"""

        if self.t_disc > 0:
            self.time_disconnected = time.time() - self.t_disc

        if not utility.validate_ip(self.ip) or not utility.validate_ip(self.lmc_ip):
            raise InvalidIpException

        logging.info("Connecting...")

        # Initial connection i.e. change from off to on
        if self.state == State.OFF:
            self.state = State.ON
            time.sleep(1 * config.time_speed)
            self.time_start = time.time()

            if self.programmed:
                self.state = State.PROGRAMMED
                self.timer = Timer(1.0, self.update_time)
                self.timer.start()

        if initialise and self.state == State.PROGRAMMED:
            self.initialise()

        logging.info("Connect Successful")
        return True

    @throws_exceptions(BoardException, DisconnectFailedException)
    @accepts(bool, State.ON, State.PROGRAMMED, State.INITIALISED)
    def disconnect(self):
        """ Disconnect board"""

        # Store temperatures of all devices
        for i in range(0, len(self.device_list)):
            self.device_list[i].temp_disconnect = self.device_list[i].get_temperature(self.time_start,
                                                                                      self.time_disconnected)
        if self.timer is not None:
            self.timer.cancel()

        if self.data_timer is not None:
            self.data_timer.cancel()

        # Store time of disconnect
        self.t_disc = time.time()

        # Change state
        self.state = State.OFF

        logging.info("Disconnected")

        return True

    @throws_exceptions(BoardException)
    @accepts(bool, State.ON, State.PROGRAMMED, State.INITIALISED)
    def program_cpld(self, bitfile):
        pass

    @throws_exceptions(DownloadFailedException, ParseFailedException, InvalidFileException, BoardException)
    @accepts(bool, State.ON, State.PROGRAMMED, State.INITIALISED)
    def program_fpgas(self, bitfile):
        """ Program FPGA's
            :param bitfile: file containing all xml files"""

        logging.info("Programming Board...")
        # self._download_files(bitfile)
        self.programmed = True

        time.sleep(3 * config.time_speed)

        self.state = State.PROGRAMMED

        logging.info("Programmed Successful")
        return True

    @throws_exceptions(BoardException, InitialiseFailedException)
    @accepts(bool, State.PROGRAMMED, State.INITIALISED)
    def initialise(self, enable_test=False):
        # check if board is programmed
        try:
            if not self.programmed:
                raise BoardException("Cannot initialise board which is not programmed.")

            logging.info("Initialising...")

            if self.timer is not None:
                self.timer.cancel()

            self.update_time()
            self.timer = Timer(1.0, self.update_time)
            self.timer.start()

            self.state = State.INITIALISED
            time.sleep(15 * config.time_speed)

            logging.info("Initialised Successful")
            return True

        except BoardException as e:
            logging.error(e.message)

    @throws_exceptions(BoardException)
    def get_state(self):
        """ Returns current state of board"""
        return self.state

    @throws_exceptions(BoardException)
    def get_ip(self):
        return self.ip

    @throws_exceptions(BoardException)
    @accepts(bool, State.PROGRAMMED, State.INITIALISED)
    def configure_10g_core(self, core_id, src_mac=None, src_ip=None, dst_mac=None, dst_ip=None, src_port=None,
                           dst_port=None):
        """ Configure a 10G core
            :param core_id: 10G core ID
            :param src_mac: Source MAC address
            :param src_ip: Source IP address
            :param dst_mac: Destination MAC address
            :param dst_ip: Destination IP address
            :param src_port: Source port
            :param dst_port: Destination port"""

        try:
            if core_id >= 4:
                core_id = core_id - 4
                device = self.fpga1_device
            else:
                device = self.fpga0_device

            # Configure Core
            if src_mac is not None:
                device.tpm_10g_core[core_id].set_src_mac(src_mac)
            if src_ip is not None:
                device.tpm_10g_core[core_id].set_src_ip(src_ip)
            if dst_mac is not None:
                device.tpm_10g_core[core_id].set_dst_mac(dst_mac)
            if dst_ip is not None:
                device.tpm_10g_core[core_id].set_dst_ip(dst_ip)
            if src_port is not None:
                # device.tpm_10g_core[core_id].set_src_port(src_port)
                pass
            if dst_port is not None:
                # device.tpm_10g_core[core_id].set_dst_port(dst_port)
                pass

            return True

        except Exception as e:
            logging.error(e.message)
            return False

    @throws_exceptions(BoardException)
    @accepts(dict, State.PROGRAMMED, State.INITIALISED)
    def get_10g_core_configuration(self, core_id):
        """ Get the configuration for a 10g core
        :param core_id: Core ID """

        if core_id >= 4:
            core_id = core_id - 4
            device = self.fpga1_device
        else:
            device = self.fpga0_device

        return {'src_mac': device.tpm_10g_core[core_id].get_src_mac(),
                'src_ip': device.tpm_10g_core[core_id].get_src_ip(),
                'dst_ip': device.tpm_10g_core[core_id].get_dst_ip(),
                'dst_mac': device.tpm_10g_core[core_id].get_dst_mac(),
                # 'src_port': device.tpm_10g_core[core_id].get_src_port(),
                # 'dst_port': device.tpm_10g_core[core_id].get_dst_port()
                }

    @throws_exceptions(BoardException)
    @accepts(int, State.ON, State.PROGRAMMED, State.INITIALISED)
    def get_fpga_time(self, device):
        """ Return time from FPGA
            :param device: FPGA to get time from """

        return int(time.time())
        try:
            if device.value == Device.FPGA_1:
                return int(self.fpga0_device.time_value)
            elif device.value == Device.FPGA_2:
                return int(self.fpga1_device.time_value)
            else:
                return 0
                # raise BoardException("Invalid device specified")

        except BoardException as e:
            logging.error(e.message)
            return 0

    @throws_exceptions(BoardException)
    @accepts(bool, State.PROGRAMMED, State.INITIALISED)
    def set_fpga_time(self, device, device_time):

        try:
            if device.value == Device.FPGA_1:
                self.fpga0_device.set_time(device_time)
            elif device.value == Device.FPGA_2:
                self.fpga1_device.set_time(device_time)
            else:
                raise BoardException("Invalid device specified")

        except BoardException as e:
            logging.error(e.message)

    @throws_exceptions(BoardException)
    @accepts(bool, State.PROGRAMMED, State.INITIALISED)
    def update_time(self):
        """ Sets time of FPGA devices"""
        device_time = int(time.time())

        # set to device_time rather than time.time() to ensure both fpga devices have same time
        self.fpga0_device.set_time(device_time)
        self.fpga1_device.set_time(device_time)

        if self.timer is not None:
            self.timer = Timer(1.0, self.update_time)
            self.timer.start()

    @throws_exceptions(BoardException)
    @accepts(list, State.PROGRAMMED, State.INITIALISED)
    def get_fpga_timestamp(self, device):
        """ Get timestamp from FPGA
            :param device: FPGA to read timestamp from """
        try:
            # if type(device) is not FPGA:
                # raise BoardException("Invalid device specified")

            # key = device.key + ".pps_manager.timestamp_read_val"
            # return self.register_list[key]['value']
            return 0

        except BoardException as e:
            logging.error(e.message)

    @throws_exceptions(MaxVoltageExceededException, BoardException)
    @accepts(int, State.ON, State.PROGRAMMED, State.INITIALISED)
    def get_voltage(self):
        """Get Voltage
           Voltage can only be accessible when running"""

        voltage = random.uniform(config.voltage['low'], config.voltage['high'])

        return round(voltage, 2)

    @throws_exceptions(MaxCurrentExceededException, BoardException)
    @accepts(int, State.PROGRAMMED, State.INITIALISED)
    def get_current(self):
        """Get Current
           Current can only be accessible when programme"""
        current = random.uniform(config.current['low'], config.current['high'])
        return round(current, 2)

    @throws_exceptions(BoardException)
    def is_programmed(self):
        """ Function that returns true if board is programmed"""
        return self.programmed

    @accepts(State.ON, State.PROGRAMMED, State.INITIALISED)
    def get_adc_rms(self):

        adc_power_dict = {}

        for i in range(16):
            x = random.uniform(10, 35)
            y = random.uniform(x-5, x+5)

            power_dict = {'x': x, 'y': y}
            adc_power_dict.update({i: power_dict})

        return adc_power_dict

    # @throws_exceptions(AdcPowerExceededException, BoardException)
    # @accepts(int, State.INITIALISED)
    # def get_adc_power(self, adc_id, device):
    #     """Get ADC Power
    #        ADC Power can only be accessible when running"""
    #     adc_power = device.get_adc_power(adc_id)
    #
    #     if adc_power > config.devices[device.index][device.key]['Adc_Power']['high'] + 0.1:
    #         raise AdcPowerExceededException(adc_power)
    #
    #     return adc_power

    @throws_exceptions(MaxTempExceededException, BoardException)
    @accepts(int, State.ON, State.PROGRAMMED, State.INITIALISED)
    def get_temperature(self):
        """Get temperature of device
           Device temperature can only be accessible if board is programmed"""
        temperature = self.cpld_device.get_temperature(self.time_start, self.time_disconnected)

        if temperature > config.devices[self.cpld_device.index][self.cpld_device.key]['Temperature']['max'] + 0.1:
            raise MaxTempExceededException(temperature)

        return round(temperature, 4)

    @throws_exceptions(MaxTempExceededException, BoardException)
    @accepts(int, State.ON, State.PROGRAMMED, State.INITIALISED)
    def get_fpga0_temperature(self):
        """Get temperature of device
           Device temperature can only be accessible if board is programmed"""
        temperature = self.fpga0_device.get_temperature(self.time_start, self.time_disconnected)

        if temperature > config.devices[self.fpga0_device.index][self.fpga0_device.key]['Temperature']['max'] + 0.1:
            raise MaxTempExceededException(temperature)

        return temperature

    @throws_exceptions(MaxTempExceededException, BoardException)
    @accepts(int, State.ON, State.PROGRAMMED, State.INITIALISED)
    def get_fpga1_temperature(self):
        """Get temperature of device
           Device temperature can only be accessible if board is programmed"""
        temperature = self.fpga1_device.get_temperature(self.time_start, self.time_disconnected)

        if temperature > config.devices[self.fpga1_device.index][self.fpga1_device.key]['Temperature']['max'] + 0.1:
            raise MaxTempExceededException(temperature)

        return temperature

    @throws_exceptions(BoardException)
    def get_phase_terminal_count(self):
        """ Get phase terminal count"""
        return self.phase_terminal_count if self.phase_terminal_count else 0

    @throws_exceptions(BoardException)
    def set_phase_terminal_count(self, value):
        """ Set phase terminal count
            :param value: value"""
        self.phase_terminal_count = value

    @staticmethod
    @throws_exceptions(BoardException)
    def get_pps_delay():
        return 0

    @staticmethod
    @throws_exceptions(BoardException)
    def _delay_calc(current_delay, current_tc, ref_low, ref_hi):
        """ Calculate delay
            :param current_delay: current delay
            :param current_tc: current phase register terminal count
            :param ref_low: low reference
            :param ref_hi: high reference"""

        for n in range(5):
            if current_delay <= ref_low:
                new_delay = current_delay + n * 40 / 5
                new_tc = (current_tc + n) % 5

                if new_delay >= ref_low:
                    return new_tc

            elif current_delay >= ref_hi:
                new_delay = current_delay - n * 40 / 5
                new_tc = current_tc - n

                if new_tc < 0:
                    new_tc += 5

                if new_delay <= ref_hi:
                    return new_tc

            else:
                return current_tc

    @throws_exceptions(BoardException)
    def wait_pps_event(self):
        """ Wait until time value of FPGA device changes"""
        t0 = int(self.fpga1_device.time_value)

        while True:
            if t0 == int(self.fpga1_device.time_value):
                break

            logging.info(t0)
            time.sleep(0.1)

    @throws_exceptions(BoardException)
    def _download_files(self, bitfile):
        """ Retrieve the structures of the xml files and use them to build a register list
            :param bitfile: file containing all xml structures of devices"""

        structure = xmlParse.get_device_structure(bitfile)

        for device in self.device_list:
            device.set_device_structure(structure[device.key])
            device.update_device_registers()

        # merging dicts
        self.register_list = utility.merge_dictionaries(self.device_list)

    @throws_exceptions(FailedToGetRegisterListException, BoardException)
    @accepts(bool, State.PROGRAMMED, State.INITIALISED)
    def get_register_list(self, load_values=''):
        """ Returns all registers belonging to CPLD and FPGA.
            Only accessible when board is programmed."""
        # pprint(dict(self.register_list))
        return dict(self.register_list)

    @throws_exceptions(RegisterNotFoundException, BoardException)
    @accepts(list, State.PROGRAMMED, State.INITIALISED)
    def read_register(self, register_name, n, offset, device):
        """ Function that gets the value of a register given its name and device
            :param register_name: name of register
            :param n: number of words to read
            :param offset: memory address offset to read from
            :param device: device to which the register belongs"""

        try:
            # Validate number of words i.e. n
            if n < 0:
                raise InvalidNumOfWordsException

            # Full name of register i.e. DeviceName.RegisterName
            full_register_name = '%s.%s' % (device, register_name)

            # Set to lower case
            full_register_name = full_register_name.lower()

            # Check if register exists in dictionary
            if full_register_name not in self.register_list:
                raise RegisterNotFoundException

            # Retrieve list of values
            values = self.register_list[full_register_name]['value']

            # Return first n values from the list
            return values[:n] if type(values) is list else values

        except InvalidNumOfWordsException as e:
            logging.error(e.message)

        except RegisterNotFoundException as e:
            logging.error(e.message)

    @throws_exceptions(RegisterNotFoundException, BoardException)
    @accepts(bool, State.PROGRAMMED, State.INITIALISED)
    def write_register(self, register_name, values, offset, device):
        """ Function that writes a list of values to a register
            :param register_name: Name of the register
            :param values: list of values to write to register
            :param offset: address offset of register
            :param device: device to which the register falls under"""

        # No use for offset in simulator

        # Full name of register i.e. DeviceName.RegisterName
        full_register_name = '%s.%s' % (device, register_name)

        # Set to lower case
        full_register_name = full_register_name.lower()

        try:
            # Check if register exists in dictionary
            if full_register_name not in self.register_list:
                raise RegisterNotFoundException

            size = int(self.register_list[full_register_name]['size'])

            # Check is amount of values is equal to register's size
            if len(values) > size:
                logging.error("Number of elements cannot be greater than size of register i.e. %s" % size)
                return False

            # Check if value field is None
            if self.register_list[full_register_name]['value'] is not 0:
                # check if there is enough space to fit values without deleting others
                if len(values) <= size - len(self.register_list[full_register_name]['value']) and values:
                    self.register_list[full_register_name]['value'] += values
                else:
                    # overwrite previous values
                    self.register_list[full_register_name]['value'] = values

            else:
                # Set values
                self.register_list[full_register_name]['value'] = values

        except RegisterNotFoundException as e:
            logging.error(e.message)

        except Exception as e:
            logging.error("Unexpected exception.\nMessage: %s" % e.message)
            return False

        return True

    @throws_exceptions(AddressNotFoundException, BoardException)
    @accepts(list, State.ON, State.PROGRAMMED, State.INITIALISED)
    def read_address(self, address, n):
        """ Function that returns the value of a register given its address.
            :param address: Memory address to read from
            :param n: Number of words to read"""

        # Validate number of words i.e. n
        if n < 0:
            logging.error("Number of words needs to be a positive integer")
            return []
        try:
            try:
                # Search for register with given address and store the register's value in values.
                values = (self.register_list[item]['value']
                          for item in self.register_list
                          if self.register_list[item]['address'] == address).next()

                if values is None:
                    logging.info("Register does not have a value")
                    return []

                # Log and return the first n values
                return values[:n] if type(values) is list else values

            except StopIteration:
                raise AddressNotFoundException

        except AddressNotFoundException as e:
            logging.error(e.message)
            return []

        except Exception as e:
            logging.error("Unexpected exception.\nMessage: %s" % e.message)
            return []

    @throws_exceptions(AddressNotFoundException, BoardException)
    @accepts(bool, State.PROGRAMMED, State.INITIALISED)
    def write_address(self, address, values):
        """ Function that writes a list of values to a register having the given address
            :param address: Memory address to write to
            :param values: Values to write"""
        try:
            # Search for register with given address
            register = (self.register_list[item]
                        for item in self.register_list
                        if self.register_list[item]['address'] == address).next()

            # Check is amount of values is equal to register's size
            if len(values) > int(register['size']):
                logging.error("Number of elements cannot be greater than size of register i.e. %s" % (register['size']))
                return False

            # Check if value field is None
            if register['value'] is not 0:
                # Check if there is enough space to fit values without deleting previous values
                if len(values) <= int(register['size']) - len(register['value']) and values:
                    register['value'] += values
                else:
                    # overwrite
                    register['value'] = values

            else:
                # Set values
                register['value'] = values

            return True

        except StopIteration:
            raise AddressNotFoundException

        except AddressNotFoundException as e:
            logging.error(e.message)
            return []

        except Exception as e:
            logging.error("Unexpected exception.\nMessage: %s" % e.message)
            return []

    # ----------------------DATA AND OTHER EMPTY FUNCTIONS---------------------------

    def set_lmc_download(self, mode, payload_length=1024, src_ip=None, lmc_mac=None):
        pass

    def set_lmc_integrated_download(self, mode, channel_payload_length, beam_payload_length):
        pass

    def set_station_id(self, station_id, tile_id):
        self.station_id = station_id
        self.tile_id = tile_id

    def tweak_transceivers(self):
        pass

    def check_pending_data_requests(self):
        pass

    def set_channeliser_truncation(self, trunc):
        pass

    def configure_station_beamformer(self, nof_tiles, first_tile=False, start=False):
        pass

    def configure_tile_beamformer(self, regions, antenna_tapering=200.0):
        pass

    def sync_fpgas(self):
        pass

    def check_synchronization(self):
        pass

    def set_c2c_read_channel(self, channel):
        pass

    def calibrate_fpga_to_cpld(self):
        pass

    def synchronised_data_operation(self, seconds=0.2, timestamp=None):
        pass

    def synchronised_beamformer_coefficients(self, timestamp=None, seconds=0.2):
        pass

    def configure_integrated_channel_data(self, integration_time=0.5):
        self.data = IntegratedChannelisedData(self.lmc_ip, self.lmc_port, 0, 0, self.epoch_time)
        self.data_timer = Timer(integration_time, self.send_data).start()

    def send_data(self):
        print "Sending data"
        self.data.send_data(1)
        # self.data.send_data((self.epoch_time - int(time.time())) * 1e9)
        self.data_timer = Timer(1, self.send_data).start()

    def configure_integrated_beam_data(self, integration_time=0.5):
        pass

    def configure_integrated_station_beam_data(self, integration_time=0.5):
        pass

    def stop_integrated_beam_data(self):
        pass

    def stop_integrated_station_beam_data(self):
        pass

    def stop_integrated_channel_data(self):
        pass

    def stop_integrated_data(self):
        pass

    def start_acquisition(self, start_time=None, delay=2):
        # self.logger.info("SETTING EPOCH TIME")
        self.epoch_time = time.time()
        # self.logger.info(self.epoch_time)

    def _send_raw_data(self, sync=False, period=0, timestamp=None, seconds=0.2):
        pass

    def send_raw_data(self, sync=False, period=0, timeout=0, timestamp=None, seconds=0.2):
        pass

    def send_raw_data_synchronised(self, period=0, timeout=0, timestamp=None, seconds=0.2):
        pass

    def stop_raw_data(self):
        pass

    def _send_channelised_data(self, number_of_samples=128, period=0, timestamp=None, seconds=0.2):
        pass

    def send_channelised_data(self, number_of_samples=128, period=0, timeout=0, timestamp=None, seconds=0.2):
        pass

    def stop_channelised_data(self):
        pass

    def _send_beam_data(self, period=0, timestamp=None, seconds=0.2):
        pass

    def send_beam_data(self, period=0, timeout=0, timestamp=None, seconds=0.2):
        pass

    def stop_beam_data(self):
        pass

    def send_channelised_data_continuous(self, channel_id, number_of_samples=128, wait_seconds=0,
                                         timeout=0, timestamp=None, seconds=0.2):
        pass

    def stop_channelised_data_continuous(self):
        pass

    def schedule_stop_data_transmission(self, timeout=0):
        pass

    def stop_data_transmission(self):
        pass


if __name__ == '__main__':
    tile = TpmSimulator()
    tile.connect()
    tile.program_fpgas('files/bitfile')
    print(tile.is_programmed())
    tile.initialise()
    tile.configure_integrated_channel_data(integration_time=0.1)
    time.sleep(500)
    tile.disconnect()