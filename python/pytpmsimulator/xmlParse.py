import xml.etree.ElementTree as Et
import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
permission_dict = {'r': "READ", 'w': "WRITE", 'rw': "READ/WRITE"}


def _get_permission(permission):
    """ Returns string according to register's permission
        :param permission: register's permission in xml file i.e. r/w/rw"""
    return permission_dict[permission]


def _process_bits_and_shift(mask):
    if not mask:
        return None

    shift = 0
    bits = bin(mask).count('1')

    while mask % 2 == 0:
        mask = mask >> 1
        shift += 1

    return bits, shift


def _add_hex(a, b):
    a = int(a, 16)
    b = int(b, 16)
    return hex(a+b)


def get_device_structure(filepath):
    """ Returns structure of xml file represented as a dict of dicts
        :param filepath: specifies the location of the xml files"""

    tree = Et.parse(filepath)

    root = tree.getroot()

    # Adding device info

    structure = {}

    for device in root:

        try:

            device_dict = {
                'id': device.attrib['id'].lower(),
                'address': '0x00000000' if 'address' not in device.attrib else device.attrib['address'],
                'components': {}
            }

        except Exception as e:
            logging.error("Error parsing xml file attributes.\nMessage: " % e.message)

        # Adding all components info
        for component in device:

            try:
                comp = {
                    'id': '%s.%s' % (device_dict['id'], component.attrib['id'].lower()),
                    'address': _add_hex(device_dict['address'], (component.attrib['address'])),
                    'byte_size': None if 'byte_size' not in component.attrib else component.attrib['byte_size'],
                    'registers': {}
                }

            except ValueError:
                logging.error("Error parsing xml file attributes.")

            # Adding all registers info
            for register in component:

                try:
                    reg = {
                        'id': '%s.%s' % (comp['id'], register.attrib['id'].lower()),
                        'address': _add_hex(comp['address'], register.attrib['address']),
                        'type': None,
                        'device': device_dict['id'].lower(),
                        'permission': None if 'permission' not in register.attrib else _get_permission(
                            register.attrib['permission']),
                        'size': 1 if 'size' not in register.attrib else register.attrib['size'],
                        'bitmask': None if 'mask' not in register.attrib else int(register.attrib['mask'], 16),
                        'bit fields': {},
                        'value': 0,
                        'description': register.attrib['description']
                    }

                except Exception as e:
                    logging.error("Error parsing xml file attributes.\nMessage: " % e.message)

                if _process_bits_and_shift(reg['bitmask']) is not None:
                    bits, shift = _process_bits_and_shift(reg['bitmask'])
                    reg.update({'bits': bits, 'shift': shift})

                # Adding all bits info
                for register_bit in register:

                    try:
                        bit = {
                            'id': '%s.%s' % (reg['id'], register_bit.attrib['id'].lower()),
                            'device': device_dict['id'].lower(),
                            'permission': _get_permission(register_bit.attrib['permission']),
                            'size': 1 if 'size' not in register_bit.attrib else register_bit.attrib['size'],
                            'bitmask': None if 'mask' not in register_bit.attrib else int(register_bit.attrib['mask'], 16),
                            'description': register_bit.attrib['description'],
                            'value': 0
                        }

                    except Exception as e:
                        logging.error("Error parsing xml file attributes.\nMessage: " % e.message)

                    if _process_bits_and_shift(bit['bitmask']) is not None:
                        bits, shift = _process_bits_and_shift(bit['bitmask'])
                        bit.update({'bits': bits, 'shift': shift})

                    # Adding bit dictionary to register info
                    reg['bit fields'].update({'%s.%s' % (reg['id'], register_bit.attrib['id']): bit})

                # Adding register dictionary to component info
                comp['registers'].update({'%s.%s' % (comp['id'], register.attrib['id']): reg})

            # Adding component dictionary to device info
            device_dict['components'].update({'%s.%s' % (device_dict['id'], component.attrib['id']): comp})

        structure.update({'%s' % (device_dict['id']): device_dict})

    return structure
