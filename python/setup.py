from setuptools import setup

setup(
    name='simulator',
    version='0.1',
    packages=['pytpmsimulator', 'pytpmsimulator.files', 'test'],
    url='https://bitbucket.org/aavslmc/tpm-simulator',
    license='',
    author='Michael Farrell',
    description='',
    install_requires=[],
    test_suite='test'
)
