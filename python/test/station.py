#! /usr/bin/env python
from pytpmsimulator.device import Device

from pytpmsimulator.tpmsimulator import TpmSimulator as Tile
from threading import Thread
import threading
import logging
import time

# TODO: - Beamformer must be changed to work at station level, and coefficient download
#         must be synchronised across boards
#       - TPMs must know the number of contributing antennas generating the beam
#         all beam coefficients are 0 (fpga1.regfile.contributing_antennas)

# Define CSP ingest network parameters
csp_ingest_network = {0: {"mac": 0x0,
                          "ip": "0.0.0.0",
                          "port": 0xF0D1},
                      1: {"mac": 0x0,
                          "ip": "0.0.0.0",
                          "port": 0xF0D1},
                      2: {"mac": 0x0,
                          "ip": "0.0.0.0",
                          "port": 0xF0D1},
                      3: {"mac": 0x0,
                          "ip": "0.0.0.0",
                          "port": 0xF0D1},
                      4: {"mac": 0x0,
                          "ip": "0.0.0.0",
                          "port": 0xF0D1},
                      5: {"mac": 0x0,
                          "ip": "0.0.0.0",
                          "port": 0xF0D1},
                      6: {"mac": 0x0,
                          "ip": "0.0.0.0",
                          "port": 0xF0D1},
                      7: {"mac": 0x0,
                          "ip": "0.0.0.0",
                          "port": 0xF0D1},
                      }


class Station(object):
    """ Class representing an AAVS station """

    def __init__(self, station_id, port=10000, lmc_ip="10.0.10.1", lmc_port=4660):
        """ Class constructor
         :param station_id: Logical station ID
         :param port: Tile UCP port
         :param lmc_ip: IP to which control data will be sent
         :param lmc_port: Port to which control data will be sent """
        self._port = port
        self._lmc_ip = lmc_ip
        self._lmc_port = lmc_port
        self._station_id = station_id
        self._tiles = []

        self._seconds = 0.2

    def add_tile(self, ip):
        """ Add a new tile to the station
        :param ip: Tile IP to be added to station """
        self._tiles.append(Tile(ip, self._port, self._lmc_ip, self._lmc_port))

    def connect(self, initialise=False, program=False, bitfile=None,
                program_cpld=False, enable_test=False, channel_truncation=0,
                beam_integration_time=-1, channel_integration_time=-1):
        """ Initialise all tiles """

        def threaded_connect(station_tile):
            """ Internal connect method to thread connection """

            # If required, program the tiles
            if program:
                logging.info("Programming Tile")
                station_tile.program_fpgas(bitfile)

            # If required, initialise the tiles
            if initialise:
                logging.info("Initialising Tile")
                station_tile.initialise(enable_test=enable_test)

                # Set channeliser trunction
                station_tile.set_channeliser_truncation(channel_truncation)

                # Configure channel and beam integrated data
                station_tile.stop_integrated_data()
                if channel_integration_time != -1:
                    station_tile.configure_integrated_channel_data(channel_integration_time)

                if beam_integration_time != -1:
                    station_tile.configure_integrated_beam_data(beam_integration_time)

            # Connect to the tile
            station_tile.connect()

        def program_cpld_thread(station_tile):
            """ Update tile CPLD """
            # tile.connect()
            station_tile.program_cpld(bitfile)

        # Create thread for each initialisation
        target = threaded_connect
        if program_cpld:
            target = program_cpld_thread

        threads = []
        for station_tile in self._tiles:
            station_tile.connect()
            thread = Thread(target=target, args=(station_tile,))
            thread.name = "{}".format(station_tile.get_ip())
            threads.append(thread)
            thread.start()

        # Wait for threads to finish
        for thread in threads:
            thread.join()

        if initialise:
            # Form station
            logging.info("Forming station")
            # self._form_station()

            # If initialisting, synchronise all tiles in station
            logging.info("Synchronising station")
            self._station_post_synchronisation()
            self._synchronise_tiles()

    def _form_station(self):
        """ Forms the station """

        # Assign station and tile id, and tweak transceivers
        for i, tile in enumerate(self._tiles):
            tile.set_station_id(self._station_id, i)
            tile.tweak_transceivers()

        # Loop over tiles and configure 10g cores
        if len(self._tiles) > 1:
            for i, tile in range(0, len(self._tiles) - 1)[::-1]:
                for core_id in range(0, 8):
                    tile_config = self._generate_10_core_config(core_id, i)
                    next_tile_config = self._tiles[i + 1].get_10g_core_configuration(core_id)
                    tile.configure_10g_core(core_id, src_mac=tile_config['src_config'],
                                            src_ip=tile_config['src_ip'],
                                            dst_mac=next_tile_config['src_mac'],
                                            dst_ip=next_tile_config['dst_ip'],
                                            src_port=tile_config['src_port'],
                                            dst_port=tile_config['dst_port'])

            # Configure last tile in the station
            for core_id in range(0, 8):
                tile_config = self._generate_10_core_config(core_id, len(station._tiles) - 1)
                if csp_ingest_network[core_id]['ip'] != "0.0.0.0":
                    dst_mac = csp_ingest_network[core_id]['mac']
                    dst_ip = csp_ingest_network[core_id]['ip']
                    dst_port = csp_ingest_network[core_id]['port']
                else:
                    dst_config = self._generate_10_core_config(core_id, 0)
                    dst_mac = dst_config['src_mac']
                    dst_ip = dst_config['src_ip']
                    dst_port = tile_config['dst_port']

                self._tiles[-1].configure_10g_core(core_id, src_mac=tile_config['src_mac'],
                                                   src_ip=tile_config['src_ip'],
                                                   dst_mac=dst_mac,
                                                   dst_ip=dst_ip,
                                                   src_port=tile_config['src_port'],
                                                   dst_port=dst_port)
        # special case: only one TPM in the station, four 10G cores of FPGA1 are connected to four 10G cores of FPGA2
        else:
            # Configure last tile in the station
            for core_id in range(0, 8):
                tile_config = self._generate_10_core_config(core_id, 0)
                if csp_ingest_network[core_id]['ip'] != "0.0.0.0":
                    dst_mac = csp_ingest_network[core_id]['mac']
                    dst_ip = csp_ingest_network[core_id]['ip']
                    dst_port = csp_ingest_network[core_id]['port']
                else:
                    if core_id < 4:
                        core_dst = core_id + 4
                    else:
                        core_dst = core_id - 4
                    dst_config = self._generate_10_core_config(core_dst, 0)
                    dst_mac = dst_config['src_mac']
                    dst_ip = dst_config['src_ip']
                    dst_port = tile_config['dst_port']

                self._tiles[-1].configure_10g_core(core_id, src_mac=tile_config['src_mac'],
                                                   src_ip=tile_config['src_ip'],
                                                   dst_mac=dst_mac,
                                                   dst_ip=dst_ip,
                                                   src_port=tile_config['src_port'],
                                                   dst_port=dst_port)

        # Set beamformer packet length and start station beamformer if required
        for tile in self._tiles[1:]:
            tile.configure_station_beamformer(len(self._tiles), first_tile=False, start=conf.beamf_start)
        self._tiles[0].configure_station_beamformer(len(self._tiles), first_tile=True, start=conf.beamf_start)

    def _synchronise_tiles(self):
        """ Synchronise time on all tiles """

        # Repeat operation until Tiles are synchronised
        while True:
            logging.info("Synchronising tiles in station")

            # Read the current time on first tile
            curr_time = self._tiles[0].get_fpga_time(Device.FPGA_1)

            # Wait for curr_time to change (detect PPS edge)
            while self._tiles[0].get_fpga_time(Device.FPGA_1) == curr_time:
                time.sleep(0.1)

            # PPS edge detected, write time to all tiles
            curr_time = self._tiles[0].get_fpga_time(Device.FPGA_1)
            for tile in self._tiles:
                tile.set_fpga_time(Device.FPGA_1, curr_time)
                tile.set_fpga_time(Device.FPGA_2, curr_time)

            # All done, check that PPS on all boards are the same
            times = set()
            for tile in self._tiles:
                times.add(tile.get_fpga_time(Device.FPGA_1))
                times.add(tile.get_fpga_time(Device.FPGA_2))

            if len(times) == 1:
                break

        # Tiles synchronised
        logging.info("Tiles in station synchronised")

        # Set LMC data lanes
        for tile in self._tiles:
            tile.set_lmc_download("1g")
            tile.set_lmc_integrated_download("1g", 1024, 1024)

        # Start data acquisition on all boards
        delay = 2
        t0 = self._tiles[0].get_fpga_time(Device.FPGA_1)
        for tile in self._tiles:
            tile.start_acquisition(start_time=t0, delay=delay)

        t1 = self._tiles[0].get_fpga_time(Device.FPGA_1)
        if t0 + delay > t1:
            logging.info("Waiting for start acquisition")
            time.sleep((t0 + delay) - t1)
        else:
            logging.error("Start data acquisition not synchronised! Rerun intialisation")
            exit(-1)

    def _station_post_synchronisation(self):
        """ Post tile configuration synchronization """

        self._tiles[0].wait_pps_event()

        current_tc = [tile.get_phase_terminal_count() for tile in self._tiles]
        delay = [tile.get_pps_delay() for tile in self._tiles]

        for n in range(len(self._tiles)):
            self._tiles[n].set_phase_terminal_count(self._tiles[n]._delay_calc(delay[n], current_tc[n],
                                                                               16, 24))

        self._tiles[0].wait_pps_event()

        current_tc = [tile.get_phase_terminal_count() for tile in self._tiles]
        delay = [tile.get_pps_delay() for tile in self._tiles]

        for n in range(len(self._tiles)):
            self._tiles[n].set_phase_terminal_count(self._tiles[n]._delay_calc(delay[n], current_tc[n],
                                                                               delay[0] - 4, delay[0] + 4))

        self._tiles[0].wait_pps_event()

        delay = [tile.get_pps_delay() for tile in self._tiles]
        logging.info("Finished station post synchronisation ({})".format(delay))

    def _generate_10_core_config(self, core_id, tile_number):
        """ Generate configuration for a particular core
         :param core_id: 10G lane to configure
         :param tile_number: Logical tile ID within station """
        return {'src_mac': int("0x62%0*X%0*X%0*X" % (2, core_id, 4, self._station_id, 4, tile_number), 16),
                'src_ip': "192.{}.{}.{}".format(core_id, self._station_id, tile_number + 1),
                'src_port': 0xF0D0,
                'dst_port': 0xF0D1}

    # ------------------------------------ DATA OPERATIONS -------------------------------------------

    def send_raw_data(self, sync=False, period=0, timeout=0):
        """ Send raw data from all Tiles """
        self._wait_available()
        t0 = self._tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self._tiles:
            tile.send_raw_data(sync=sync, period=period, timeout=timeout, timestamp=t0, seconds=self._seconds)
        return self._check_data_sync(t0)

    def send_raw_data_synchronised(self, period=0, timeout=0):
        """ Send synchronised raw data from all Tiles """
        self._wait_available()
        t0 = self._tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self._tiles:
            tile.send_raw_data_synchronised(period=period, timeout=timeout, timestamp=t0, seconds=self._seconds)
        return self._check_data_sync(t0)

    def send_channelised_data(self, number_of_samples=128, period=0, timeout=0):
        """ Send channelised data from all Tiles """
        self._wait_available()
        t0 = self._tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self._tiles:
            tile.send_channelised_data(number_of_samples=number_of_samples, period=period,
                                       timeout=timeout, timestamp=t0, seconds=self._seconds)
        return self._check_data_sync(t0)

    def send_beam_data(self, period=0, timeout=0):
        """ Send beam data from all Tiles """
        self._wait_available()
        t0 = self._tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self._tiles:
            tile.send_beam_data(period=period, timeout=timeout, timestamp=t0, seconds=self._seconds)
        return self._check_data_sync(t0)

    def send_csp_data(self, samples_per_packet, number_of_samples):
        """ Send CSP data from all Tiles """
        self._wait_available()
        t0 = self._tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self._tiles:
            tile.send_csp_data(samples_per_packet=samples_per_packet, number_of_samples=number_of_samples,
                               timestamp=t0, seconds=0.5)
        return self._check_data_sync(t0)

    def send_channelised_data_continuous(self, channel_id, number_of_samples=128, timeout=0):
        """ Send continuous channelised data from all Tiles """
        self._wait_available()
        t0 = self._tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self._tiles:
            tile.send_channelised_data_continuous(channel_id=channel_id, number_of_samples=number_of_samples,
                                                  timeout=timeout, timestamp=t0, seconds=self._seconds)
        return self._check_data_sync(t0)

    def stop_data_transmission(self):
        """ Stop data transmission """
        for tile in self._tiles:
            tile.stop_data_transmission()

    def stop_integrated_data(self):
        """ Stop integrated data transmission """
        for tile in self._tiles:
            tile.stop_integrated_data()

    def _wait_available(self):
        """ Make sure all boards can send data """
        while any([tile.check_pending_data_requests() for tile in self._tiles]):
            logging.debug("Waiting for pending data requests to finish")
            time.sleep(0.1)

    def _check_data_sync(self, t0):
        """ Check whether data synchronisation worked """
        delay = self._seconds * (1 / (1080 * 1e-9) / 256)
        timestamps = [tile.get_fpga_timestamp(Device.FPGA_1) for tile in self._tiles]
        logging.debug("Data sync check: timestamp={}, delay={}".format(str(timestamps), delay))
        return all([(t0 + delay) > t1 for t1 in timestamps])

        # ------------------------------------------- OVERLOADED FUNCTIONS ---------------------------------------

    def __getitem__(self, key):
        """ Read register across all tiles """
        return [tile.tpm[key] for tile in self._tiles]

    def __setitem__(self, key, value):
        """ Write register across all tiles """
        for tile in self._tiles:
            tile.tpm[key] = value


if __name__ == "__main__":
    from optparse import OptionParser
    from sys import argv, stdout

    parser = OptionParser(usage="usage: %station [options]")
    parser.add_option("--port", action="store", dest="port",
                      type="int", default="10000", help="Port [default: 10000]")
    parser.add_option("--lmc_ip", action="store", dest="lmc_ip",
                      default="10.0.10.200", help="IP [default: 10.0.10.200]")
    parser.add_option("--lmc_port", action="store", dest="lmc_port",
                      type="int", default="4660", help="Port [default: 4660]")
    parser.add_option("-f", "--bitfile", action="store", dest="bitfile",
                      default=None, help="Bitfile to use (-P still required) [default: Local bitfile]")
    parser.add_option("-t", "--tiles", action="store", dest="tiles",
                      default="tpm-1", help="Tiles to add to station [default: tpm-1]")
    parser.add_option("-P", "--program", action="store_true", dest="program",
                      default=False, help="Program FPGAs [default: False]")
    parser.add_option("-I", "--initialise", action="store_true", dest="initialise",
                      default=False, help="Initialise TPM [default: False]")
    parser.add_option("-C", "--program_cpld", action="store_true", dest="program_cpld",
                      default=False, help="Update CPLD firmware (requires -f option)")
    parser.add_option("-T", "--enable-test", action="store_true", dest="enable_test",
                      default=False, help="Enable test pattern (default: False)")
    parser.add_option("--chan-trunc", action="store", dest="chan_trunc",
                      default=2, type="int", help="Channeliser truncation [default: 2]")
    parser.add_option("-B", "--beamf_start", action="store_true", dest="beamf_start",
                      default=False, help="Start network beamformer")
    parser.add_option("", "--channel-integration-time", action="store", dest="channel_integ",
                      type="float", default=0.5, help="Integrated channel integration time [default: 0.5s]")
    parser.add_option("", "--beam-integration-time", action="store", dest="beam_integ",
                      type="float", default=-1, help="Integrated beam integration time [default: -1]")
    (conf, args) = parser.parse_args(argv[1:])

    # Set logging
    log = logging.getLogger('')
    log.setLevel(logging.DEBUG)
    line_format = logging.Formatter("%(asctime)s - %(levelname)s - %(threadName)s - %(message)s")
    ch = logging.StreamHandler(stdout)
    ch.setFormatter(line_format)
    log.addHandler(ch)

    # Set current thread name
    threading.currentThread().name = "Station"

    # Assign tiles
    station_tiles = conf.tiles.split(',')

    # Create station
    station = Station(0, port=conf.port, lmc_ip=conf.lmc_ip, lmc_port=conf.lmc_port)

    station.add_tile('10.0.10.2')
    station.add_tile('10.0.10.3')

    station.connect(initialise=True,
                    program=True,
                    bitfile='../pytpmsimulator/files/bitfile',
                    program_cpld=False)

    # tile = Tile('10.0.10.2', 10000, '10.0.10.1', 4660)
    # station.add_tile(tile)
    #
    # for t in station_tiles:
    #    station.add_tile(t)
    #
    # # Program and initialise tiles
    # station.connect(initialise=conf.initialise,
    #                 program=conf.program,
    #                 bitfile=conf.bitfile,
    #                 enable_test=conf.enable_test,
    #                 program_cpld=conf.program_cpld,
    #                 channel_truncation=conf.chan_trunc,
    #                 beam_integration_time=conf.beam_integ,
    #                 channel_integration_time=conf.channel_integ)
